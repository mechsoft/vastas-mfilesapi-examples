﻿using MFiles.Mfws;
using MFiles.Mfws.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace Mechsoft.Vastas.Examples
{
    /// <summary>
    /// HTTP Client sınıfını kullanarak M-Files REST Servise basit çağrılar yapılabilmesi için yazılan sınıf.
    /// </summary>
    public class VastasWrapper
    {
        /// <summary>
        /// M-Files üzerinde yeni bir çalışan oluşturmaya yarayan örnek metod.
        /// </summary>
        /// <param name="token">Çağrıda kullanılacak token.</param>
        /// <returns></returns>
        public static async Task<ObjectVersion> CreateEmployeeAsync(string token)
        {
            //HttpClient Oluştur.
            var httpClient = new HttpClient();

            //Daha önceden alınan token' ı header ' a ekle.
            httpClient.DefaultRequestHeaders.Add("X-Authentication", token);

            //Çağrı Mesajı Oluştur.
            var requestmessage = new HttpRequestMessage()
            {
                Method = HttpMethod.Post,
                //Çağrı Adresi: 
                //101 -> Personel Nesne ID
                //Bknz. MFWS Documentation.pdf Sayfa: 12 Madde: 5.1.2
                RequestUri = new Uri(string.Format("{0}/objects/101", VastasConstants.RESTURI))
            };

            //Mesaj içeriğini tanımla
            requestmessage.Content = new ObjectContent<ObjectCreationInfo>(
                //Obje Yaratmak için kullanılacak nesne
                //Bknz. MFWS Documentation.pdf Sayfa: 28 Madde: 6.3
                new ObjectCreationInfo()
                {
                    PropertyValues = new List<PropertyValue>() {
                        //Sınıf Özelliği
                         new PropertyValue {PropertyDef = 100, TypedValue = new TypedValue() {DataType = MFDataType.Lookup, Lookup = new Lookup() {Item = 2, Version = -1 } } },
                         //İsim Özelliği
                       new PropertyValue {PropertyDef = 1035, TypedValue = new TypedValue() {DataType = MFDataType.Text, Value = "Gökay Kıvırcıoğlu" } },
                       //TC Kimlik Numarası Özelliği
                       new PropertyValue {PropertyDef = 1036, TypedValue = new TypedValue () {DataType = MFDataType.Text, Value = "57854855451" } },
                       //E-Mail Özelliği
                       new PropertyValue {PropertyDef = 1030, TypedValue = new TypedValue () {DataType = MFDataType.Text, Value = "riza.gokay@gmail.com" } }
                    }.ToArray()
                },
               new JsonMediaTypeFormatter());

            //Çağrıyı Gönder
            var response = await httpClient.SendAsync(requestmessage);

            //Cevap başarılı ise
            response.EnsureSuccessStatusCode();

            //Dönen cevabı ObjectVersion'a Çevir
            //Bknz. MFWS Documentation.pdf Sayfa: 31 Madde: 6.21
            var objectVersion = await response.Content.ReadAsAsync<ObjectVersion>();

            return objectVersion;

        }

        /// <summary>
        /// M-Files üzerinde var olan bir personel nesnesinin ismini güncellemek üzere kullanılan örnek metod.
        /// </summary>
        /// <param name="token">Çağrıda kullanılacak token.</param>
        /// <param name="objectId">Güncellenecek nesnenin id si</param>
        /// <param name="name">Güncellenecek isim</param>
        /// <returns>Güncellenen obje döndürülür.</returns>
        public static async Task<ObjectVersion> UpdateEmployeeNameAsync(string token, int objectId, string name)
        {
            //HttpClient Oluştur.
            var httpClient = new HttpClient();

            //Daha önceden alınan token' ı header ' a ekle.
            httpClient.DefaultRequestHeaders.Add("X-Authentication", token);

            //M-Files'da bulunan bir nesne güncellenmeden önce check-out edilmeli. Güncellendikten sonra Check-In edilmelidir. Bu örnekte
            //check-out, update, check-in olmak üzere 3 çağrı yapılmıştır.


            //Check-Out Çağrısı Mesajını Oluştur
            var checkOutMessage = new HttpRequestMessage()
            {
                Method = HttpMethod.Put,
                //Çağrı Adresi: 
                //101 -> Personel Nesne ID
                //objectId -> Update edilecek nesnenin ID'si
                //Bknz. MFWS Documentation.pdf Sayfa: 14 Madde: 5.1.4.4
                RequestUri = new Uri(string.Format("{0}/objects/101/{1}/-1/checkedout", VastasConstants.RESTURI, objectId))
            };

            //Check-Out Çağrısı İçeriği belirlenir.
            checkOutMessage.Content = new ObjectContent<PrimitiveType<MFCheckOutStatus>>(
                new PrimitiveType<MFCheckOutStatus>() { Value = MFCheckOutStatus.CheckedOutToMe },
                new JsonMediaTypeFormatter());

            //Çağrıyı Gönder
            var response = await httpClient.SendAsync(checkOutMessage);

            response.EnsureSuccessStatusCode();

            //Dönen cevabı ObjectVersion'a Çevir
            //Bknz. MFWS Documentation.pdf Sayfa: 31 Madde: 6.21
            var checkedOutObject = await response.Content.ReadAsAsync<ObjectVersion>();


            //Update Çağrısı Mesajını Oluştur
            var editMessage = new HttpRequestMessage()
            {
                Method = HttpMethod.Put,
                //Çağrı Adresi: 
                //101 -> Personel Nesne ID
                //1035 -> Adı Soyadı Özelliğinin ID'si
                //Bknz. MFWS Documentation.pdf Sayfa: 17 Madde: 5.2.2
                RequestUri = new Uri(string.Format("{0}/objects/101/{1}/-1/properties/1035", VastasConstants.RESTURI, objectId))

            };

            //Update Çağrısı İçeriği belirlenir.
            editMessage.Content = new ObjectContent<PropertyValue>(
                new PropertyValue() { PropertyDef = 1035, TypedValue = new TypedValue() { DataType = MFDataType.Text, Value = name } },
            new JsonMediaTypeFormatter());

            //Çağrıyı Gönder
            response = await httpClient.SendAsync(editMessage);

            response.EnsureSuccessStatusCode();

            //Check-In Çağrısı Mesajını Oluştur
            var checkInMessage = new HttpRequestMessage()
            {
                Method = HttpMethod.Put,
                //Çağrı Adresi: 
                //101 -> Personel Nesne ID
                //{1} objectId -> Update edilecek nesnenin ID'si
                //{2} -> CheckIn edilecek nesnenin versiyonu
                //Bknz. MFWS Documentation.pdf Sayfa: 14 Madde: 5.1.4.4
                RequestUri = new Uri(string.Format("{0}/objects/101/{1}/{2}/checkedout", VastasConstants.RESTURI, objectId, checkedOutObject.ObjVer.Version))
            };

            //Check-In Çağrısı içeriği belirlenir.
            checkInMessage.Content = new ObjectContent<PrimitiveType<MFCheckOutStatus>>(
                new PrimitiveType<MFCheckOutStatus>() { Value = MFCheckOutStatus.CheckedIn },
                new JsonMediaTypeFormatter());

            //Çağrıyı Gönder
            response = await httpClient.SendAsync(checkInMessage);

            response.EnsureSuccessStatusCode();

            //Dönen cevabı ObjectVersion'a Çevir
            //Bknz. MFWS Documentation.pdf Sayfa: 31 Madde: 6.21
            var checkedInObject = await response.Content.ReadAsAsync<ObjectVersion>();

            return checkedInObject;

        }

        /// <summary>
        /// Sunucuya bağlanıp REST çağrı işlemlerinde kullanılmak üzere token almaya yarayan metod.
        /// </summary>
        /// <returns>Token değeri</returns>
        public static async Task<PrimitiveType<string>> GetTokenAsync()
        {
            //HttpClient Oluştur.
            var httpClient = new HttpClient();

            //Çağrı mesajını oluştur.
            var requestmessage = new HttpRequestMessage()
            {
                Method = HttpMethod.Post,
                //Çağrı Adresi: 
                //Bknz. MFWS Documentation.pdf Sayfa: 21 Madde: 5.6.5
                RequestUri = new Uri(string.Format("{0}/server/authenticationtokens.aspx", VastasConstants.RESTURI))
            };
            //Çağrı mesajı içeriğini belirle.
            requestmessage.Content = new ObjectContent<Authentication>(
                new Authentication() { Username = VastasConstants.ACCOUNT, Password = VastasConstants.PASSWORD, VaultGuid = VastasConstants.VAULTGUID },
                new JsonMediaTypeFormatter());

            //Çağrıyı Gönder
            var response = await httpClient.SendAsync(requestmessage);

            response.EnsureSuccessStatusCode();

            //Dönen cevabı PrimitiveType' a çevir.
            var token = await response.Content.ReadAsAsync<PrimitiveType<string>>();


            //Aynı Çağrı MFWSClient Kullanılarak yapıldığında;

            //var mfws = new MfwsClient(VastasConstants.RESTURI);
            //var tokenAlt = mfws.Post<PrimitiveType<string>>(
            //    "/server/authenticationtokens.aspx",
            //    new Authentication()
            //    {
            //        Username = VastasConstants.ACCOUNT,
            //        Password = VastasConstants.PASSWORD,
            //        VaultGuid = VastasConstants.VAULTGUID
            //    });
            //return tokenAlt.Value;

            return token;
        }
    }
}
