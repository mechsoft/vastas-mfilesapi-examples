﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using MFiles.Mfws.Structs;
using System.Net.Http.Formatting;
using MFiles.Mfws;
using System.Threading.Tasks;

namespace Mechsoft.Vastas.Examples
{
    class Program
    {
        static void Main(string[] args)
        {

            //Notlar 
            //Örnekler HTTPClient ile REST servise çağrılar yapılarak yapılmıştır.
            //Aynı çağrılar MFWSClient nesnesi kullanılarak da yapılabilir.

            //Token Alma
            var token = VastasWrapper.GetTokenAsync().Result;

            //Oluşturma
            var createdEmployee = VastasWrapper.CreateEmployeeAsync(token.Value).Result;

            //İsmi Güncelleme
            var updatedObject = VastasWrapper.UpdateEmployeeNameAsync(token.Value, createdEmployee.ObjVer.ID, "Rıza Gökay Kıvırcıoğlu").Result;
        }
    }
}
